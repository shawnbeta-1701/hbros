module.exports = function (grunt) {
    'use strict';
    grunt.util.linefeed = '\n';

    grunt.initConfig({
        clean: {
            main: [
                'web/themes/custom/'
                // 'themes/sw_theme/assets/',
            ],
            currentVersion: ['current_build/**'],
            currentBuild: ['build/**']
        },

        less: {

            dev: {
                options: {
                    strictMath: true,
                    outputSourceFiles: true
                },
                files: {
                    'assets/css/styles.css': 'bundles/**/*.less',
                }
            },
        },
    });

    require('load-grunt-tasks')(grunt, {scope: 'devDependencies'});
    grunt.registerTask('dev', ['less',]);

};