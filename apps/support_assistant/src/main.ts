import { platformBrowser }    from '@angular/platform-browser';

import { AppModuleNgFactory } from '../aot/prep/src/app/app.module.ngfactory';

platformBrowser().bootstrapModuleFactory(AppModuleNgFactory);
