import { Department } from './department';

export const DEPARTMENTS: Department[] = [
    {
        id: 0,
        key: 'rd',
        name: 'Research and Development',
        url: 'rd',
    },
    {
        id: 1,
        key: 'returns',
        name: 'Returns',
        url: 'returns',
    },
    {
        id: 2,
        key: 'shipping',
        name: 'Shipping',
        url: 'shipping-delivery',
    },
];