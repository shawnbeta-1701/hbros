export class Department {
    id: number;
    key: string;
    name: string;
    url: string;
}