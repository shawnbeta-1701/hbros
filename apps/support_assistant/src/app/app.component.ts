import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DepartmentService  } from '../dept/department.service';
import {Department} from "../dept/department";
import {TransmitService} from "../utility/transmit.service";

@Component({
  selector: 'my-app',
  templateUrl: '../dept/templates/choose.tpl.html',
  // template: 'welcome',
  providers: [ DepartmentService, TransmitService ],
})

export class AppComponent  implements OnInit {


  name = 'Angular';
  departments: Department[];
   rsp:  Observable<string>;
  // rsp:  string;
  mode = 'Observable';

  constructor(
      private departmentService: DepartmentService,
      private transmitService: TransmitService
  ) {

  }

  ngOnInit(): void {
    this.getTest();
  }

  getTest(): void {
    console.log('getTest');
    let test = this.transmitService.getUrl();
  }

}
