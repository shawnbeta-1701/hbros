module.exports = function (grunt) {
    'use strict';
    grunt.util.linefeed = '\n';
    RegExp.quote = function (string) {
        return string.replace(/[-\\^$*+?.()|[\]{}]/g, '\\$&');
    };

    grunt.initConfig({
        clean: {
            main: [
                'web/themes/custom/'
            ],
            currentVersion: ['current_build/**'],
            currentBuild: ['build/**']
        },

        less: {
            dev: {
                options: {
                    strictMath: true,
                    outputSourceFiles: true
                },
                files: {
                    'assets/css/styles.css': 'bundles/**/*.less',
                }
            },
        },

        copy: {
            dev: {
                files: [
                    {expand: true, cwd: 'src/', src: ['**/*.ts', '**/*.tpl.html'], dest: 'prep/src/'},
                    {expand: true, cwd: 'src/', src: ['assistant.html.twig'], dest: 'dist/web/'},
                    {expand: true, cwd: 'node_modules/core-js/client/', src: ['shim.min.js'], dest: 'dist/web/js/'},
                    {expand: true, cwd: 'node_modules/zone.js/dist/', src: ['zone.min.js'], dest: 'dist/web/js/'},

                ],
            },
        },
    });

    require('load-grunt-tasks')(grunt, {scope: 'devDependencies'});
    grunt.registerTask('dev', ['less',  'copy', ]);
};