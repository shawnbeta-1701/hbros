<?php
/**
 * Created by PhpStorm.
 * User: shawn
 * Date: 4/21/17
 * Time: 8:09 PM
 */

namespace SWD\Commerce\ProductBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CreateProductForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add()
      ->add('title', TextType::class)
      ->add('price', MoneyType::class)
      ->add('overview', TextareaType::class)
      ->add('save', SubmitType::class)
    ;
  }
}