<?php

namespace SWD\ContentBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSuperclass;


/**
 * @ORM\Entity
 * @ORM\Table(name="simple_page")
 */class SimplePage
{
  /**
   * @ORM\Column(type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */  protected $id;


  /** @ORM\Column(type="string", length=20) */
  protected $uniqueKey;

  /** @ORM\Column(type="string") */
  protected $title;

  /**
   * @ORM\Column(type="text")
   */
  protected $body;

  /** @ORM\Column(type="string") */
  protected $path_alias;

  protected $attachments = array();

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


  /**
   * Set unique key
   *
   * @param string $uniqueKey
   *
   * @return File
   */
  public function setUniqueKey($uniqueKey)
  {
    $this->uniqueKey = $uniqueKey;

    return $this;
  }

  /**
   * Get unique key
   *
   * @return string
   */
  public function getUniqueKey()
  {
    return $this->uniqueKey;
  }


    /**
     * Set title
     *
     * @param string $title
     *
     * @return File
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Set article
     *
     * @param string $body
     *
     * @return File
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

  /**
   * Set path alias
   *
   * @param string $pathAlias
   *
   * @return File
   */
  public function setPathAlias($path_alias)
  {
    $this->path_alias = $path_alias;

    return $this;
  }

  /**
   * Get path alias
   *
   * @return string
   */
  public function getPathAlias()
  {
    return $this->path_alias;
  }

  // STOP ERROR
  public function getpath_alias()
  {
    return $this->path_alias;
  }

  /**
   * Set attachment
   *
   * @param string $pathAlias
   *
   * @return File
   */
  public function setAttachment($attachment)
  {
    array_push($this->attachments, $attachment);

    return $this;
  }

  /**
   * Get path attachment
   *
   * @return array
   */
  public function getAttachments()
  {
    return $this->path_alias;
  }



}
