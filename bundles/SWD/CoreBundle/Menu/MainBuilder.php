<?php

namespace SWD\CoreBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use SWD\CoreBundle\Event\ConfigureMainMenuEvent;

class MainBuilder implements ContainerAwareInterface
{
  use ContainerAwareTrait;

  public function mainMenu(FactoryInterface $factory)
  {
    $menu = $factory->createItem('root');

    $this->container->get('event_dispatcher')->dispatch(
      ConfigureMainMenuEvent::CONFIGURE,
      new ConfigureMainMenuEvent($factory, $menu)
    );

    return $menu;
  }
}