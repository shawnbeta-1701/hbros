<?php

namespace SWD\CoreBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="relationship_reference")
 */
class RelationshipReference {

  /**
   * @ORM\Column(type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @ORM\Column(type="string", nullable=true)
   */
  private $parent;

  /**
   * @ORM\Column(type="string", nullable=true)
   */
  private $child;

  /**
   * @ORM\Column(type="string", nullable=true)
   */
  private $sibling;

  /**
   * @ORM\Column(type="string", length=100)
   */
  private $type;

    /**
     * Set parent
     *
     * @param string $parent
     *
     * @return RelationshipReference
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return string
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set child
     *
     * @param string $child
     *
     * @return RelationshipReference
     */
    public function setChild($child)
    {
        $this->child = $child;

        return $this;
    }

    /**
     * Get child
     *
     * @return string
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * Set sibling
     *
     * @param string $sibling
     *
     * @return RelationshipReference
     */
    public function setSibling($sibling)
    {
        $this->sibling = $sibling;

        return $this;
    }

    /**
     * Get sibling
     *
     * @return string
     */
    public function getSibling()
    {
        return $this->sibling;
    }

  /**
   * Set type
   *
   * @param string $type
   *
   * @return File
   */
  public function setType($type)
  {
    $this->type = $type;

    return $this;
  }

  /**
   * Get type
   *
   * @return string
   */
  public function getType()
  {
    return $this->type;
  }
}
