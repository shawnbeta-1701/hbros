<?php

namespace SWD\MediaBundle\Service;


use SWD\MediaBundle\Entity\Image;

class ImageService {

  public function processUpload($imageCollection, $imagePath, $em)
  {
    foreach ($imageCollection as $image) {
      $imageObj = new Image();
      $imageObj->setName($image->getClientOriginalName());
      $imageObj->setSrc($image->getClientOriginalName());
      $imageObj->setExtension($image->getClientOriginalExtension());
      $em->persist($imageObj);
      $image->move(
        $imagePath, $image->getClientOriginalName()
      );
    }
    $em->flush();
  }

}