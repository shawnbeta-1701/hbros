<?php
/**
 * Created by PhpStorm.
 * User: shawn
 * Date: 4/23/17
 * Time: 7:54 AM
 */

namespace SWD\AdminBundle\Event;


use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\EventDispatcher\Event;

class ConfigureAdminMenuEvent extends Event
{

  const CONFIGURE = 'swd.admin.configure_admin_menu';

  private $factory;
  private $menu;

  /**
   * @param \Knp\Menu\FactoryInterface $factory
   * @param \Knp\Menu\ItemInterface $menu
   */
  public function __construct(FactoryInterface $factory, ItemInterface $menu)
  {
    $this->factory = $factory;
    $this->menu = $menu;
  }

  /**
   * @return \Knp\Menu\FactoryInterface
   */
  public function getFactory()
  {
    return $this->factory;
  }

  /**
   * @return \Knp\Menu\ItemInterface
   */
  public function getMenu()
  {
    return $this->menu;
  }
}
