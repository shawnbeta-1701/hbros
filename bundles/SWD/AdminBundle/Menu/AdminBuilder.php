<?php


namespace SWD\AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use SWD\AdminBundle\Event\ConfigureAdminMenuEvent;

class AdminBuilder implements ContainerAwareInterface
{
  use ContainerAwareTrait;

  public function adminMenu(FactoryInterface $factory)
  {
    $menu = $factory->createItem('root');

    $this->container->get('event_dispatcher')->dispatch(
      ConfigureAdminMenuEvent::CONFIGURE,
      new ConfigureAdminMenuEvent($factory, $menu)
    );

    $menu->addChild('Admin', array('uri' => '/admin'));
    return $menu;
  }
}