<?php

namespace HBros\ContentBundle\Controller;

use HBros\CoreBundle\Menu\MenuService;
use SWD\UtilityBundle\Service\UtilityService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PageController extends Controller
{

    /**
     * @Route("/")
     * @Route("/welcome")
     */
    public function welcomePageAction()
    {
        $page = array('title' => 'Welcome to HBros');
        return $this->render('HBrosContentBundle:Pages:welcome.html.twig', array('page'=>$page));
    }


  /**
   * @Route("/about")
   * @Route("/customer-service")
   * @Route("/contact")
   */
  public function simplePageAction()
  {
      $uri = $_SERVER['REQUEST_URI'];
      $page = $this->getDoctrine()
          ->getRepository('SWDContentBundle:SimplePage')
          ->findOneBy(array('path_alias' => UtilityService::cleanString($uri)));
    return $this->render('HBrosContentBundle:Pages:simple-page.html.twig', array('page' => $page));
  }




}
