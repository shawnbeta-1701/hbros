<?php
/**
 * Created by PhpStorm.
 * User: shawn
 * Date: 5/6/17
 * Time: 9:24 PM
 */


namespace HBros\ContentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MenuController extends Controller
{

    private static $mainMenu = array(
        array('text'=>'home', 'uri'=>''),
        array('text'=>'robots', 'uri' => 'products/robots'),
        array('text'=>'about us', 'uri'=>'about'),
        array('text'=>'customer service', 'uri'=>'customer-service'),
        array('text'=>'contact us', 'uri'=>'contact'),
    );

    public function renderMainMenuAction()
    {
        return $this->render(
            'SWDCoreBundle:Menus:menu.html.twig',
            array('links'=>self::$mainMenu)
        );

    }

}