<?php

namespace HBros\ContentBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CreateSimplePageForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('title', TextType::class)
      ->add('body', TextareaType::class)
//      ->add('images', FileType::class, array(
//        'label' => 'Upload Images',
//        'mapped' => FALSE,
//        'multiple' => TRUE,
//      ))
//      ->add('type', ChoiceType::class, array(
//        'choices'  => array(
//          'legged' => 'legged',
//          'wheeled' => 'wheeled',
//          'floated' => 'floated',
//        ),
//      ))
      ->add('path_alias', TextType::class)
      ->add('save', SubmitType::class)
    ;
  }
}