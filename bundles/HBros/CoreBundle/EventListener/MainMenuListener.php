<?php

namespace HBros\CoreBundle\EventListener;

use SWD\CoreBundle\Event\ConfigureMainMenuEvent;

class MainMenuListener
{
  /**
   * @param \SWD\CoreBundle\Event\ConfigureMainMenuEvent $event
   */
  public function onMainMenuConfigure(ConfigureMainMenuEvent $event)
  {
    $menu = $event->getMenu();
    $menu->addChild('Welcome', array('uri' => '/'));
    $menu->addChild('Robots', array('uri' => '/products/robots'));
    $menu['Robots']->addChild('Add Robot', array('uri' => '/create/robot'));
      $menu->addChild('Shopping Cart', array('uri' => '/cart'));
      $menu->addChild('About', array('uri' => '/about'));
      $menu->addChild('Customer Service', array('uri' => '/customer-service'));
      $menu->addChild('Contact Us', array('uri' => '/contact'));
      $menu->addChild('Create Simple Page', array('uri' => '/create/simple-page'));

  }
}