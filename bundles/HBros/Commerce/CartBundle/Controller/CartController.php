<?php

namespace HBros\Commerce\CartBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class CartController extends Controller
{
    /**
     * @Route("/cart")
     */
    public function cartAction()
    {
        return $this->render('HBrosCommerceCartBundle:Pages:cart.html.twig');
    }
}
