<?php
/**
 * Created by PhpStorm.
 * User: shawn
 * Date: 4/29/17
 * Time: 6:41 PM
 */

namespace HBros\Commerce\ProductBundle\ProductService;


use SWD\CoreBundle\Entity\RelationshipReference;
use SWD\MediaBundle\Entity\Image;
use SWD\UtilityBundle\Service\UtilityService;

class ProductService {

  public function persistImageCollection($parentId, $imgCollection, $path, $em)
  {
    foreach ($imgCollection as $image) {
      $imageObj = new Image();
      $uniqueKey = UtilityService::getRandomString(11);
      $imageName = $image->getClientOriginalName();
      $imageObj->setUniqueKey($uniqueKey);
      $imageObj->setName($imageName);
      $imageObj->setSrc($imageName);
      $imageObj->setExtension($image->getClientOriginalExtension());
      $em->persist($imageObj);
      $image->move(
        $path, $imageName
      );

      // IF THE IMAGE HAS A PARENT SETUP A RELATIONSHIP
      if(!$parentId) return;
      $relatedEntity = new RelationshipReference();
      $relatedEntity->setParent($parentId);
      $relatedEntity->setChild($uniqueKey);
      $relatedEntity->setType('product_gallery');
      $em->persist($relatedEntity);
    }
    $em->flush();
  }

}