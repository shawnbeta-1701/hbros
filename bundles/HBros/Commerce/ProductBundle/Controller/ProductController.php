<?php

namespace HBros\Commerce\ProductBundle\Controller;

use Doctrine\ORM\Query\ResultSetMapping;
use HBros\Commerce\ProductBundle\Entity\Robot;
use HBros\Commerce\ProductBundle\Form\CreateRobotForm;
use HBros\Commerce\ProductBundle\ProductService\ProductService;
use SWD\MediaBundle\Service\ImageService;
use SWD\UtilityBundle\Service\UtilityService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class ProductController extends Controller
{

  /**
   * @Route("/products/robots/{path_alias}", name="robot_pdp")
   */
  public function showAction($path_alias)
  {
//    $robot = $this->getDoctrine()
//      ->getRepository('HBrosCommerceProductBundle:Robot')
//      ->findOneBy(array('path_alias'=>$path_alias));



    $em = $this->get('doctrine')->getManager();

    $robotQuery  = $em->createQuery("
      SELECT r
      FROM HBros\Commerce\ProductBundle\Entity\Robot r
      WHERE r.path_alias = :param
        OR r.id = :param
     ")
      ->setParameter('param', $path_alias);

    $robot = $robotQuery->getSingleResult();

    $query  = $em->createQuery("
      SELECT DISTINCT img.src
      FROM SWD\CoreBundle\Entity\RelationshipReference rr
      INNER JOIN  SWD\MediaBundle\Entity\Image img
      WHERE img.unique_key = rr.child
      WHERE rr.parent = :parent_unique_key    
     ")
      ->setParameter('parent_unique_key', $robot->getUniqueKey());

    $productImages = $query->getResult();

    return $this->render('HBrosCommerceProductBundle:Details:robots-details.html.twig', array('product'=>$robot, 'product_images' => $productImages));

  }

  /**
   * @Route("/products/robots")
   */
  public function listRobots()
  {

      $page = array (
          'title' => 'Our Robots',
      );


    $pdo = $this->container->get('db1');
    $robots = $pdo->query('SELECT r.title, r.path_alias, r.price, i.src
      FROM product_robot AS r
      INNER JOIN image AS i
        WHERE i.unique_key = (
          SELECT rr.child FROM relationship_reference AS rr
          WHERE rr.parent = r.unique_key
          LIMIT 1)')->fetchAll();




    return $this->render('HBrosCommerceProductBundle:List:robots-list.html.twig', array(
        'page' => $page,
        'robots'=>$robots));
  }



}
