<?php

namespace HBros\Commerce\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use SWD\Commerce\ProductBundle\Entity\Product;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_robot")
 */
class Robot extends Product
{

  /**
   * @ORM\Column(type="string", length=100)
   */
    private $type;


    /**
     * Set type
     *
     * @param string $type
     *
     * @return Robot
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set uniqueKey
     *
     * @param string $uniqueKey
     *
     * @return Robot
     */
    public function setUniqueKey($uniqueKey)
    {
        $this->uniqueKey = $uniqueKey;

        return $this;
    }

    /**
     * Get uniqueKey
     *
     * @return string
     */
    public function getUniqueKey()
    {
        return $this->uniqueKey;
    }


}
