<?php
/**
 * Created by PhpStorm.
 * User: shawn
 * Date: 4/22/17
 * Time: 3:03 PM
 */

namespace HBros\Commerce\ProductBundle\Entity;

use SWD\Commerce\ProductBundle\Entity\Product;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_accessory")
 */
class Accessory extends Product
{

  /**
   * @ORM\Column(type="decimal", scale=2)
   */

  private $weight;


    /**
     * Set weight
     *
     * @param string $weight
     *
     * @return Accessory
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set uniqueKey
     *
     * @param string $uniqueKey
     *
     * @return Accessory
     */
    public function setUniqueKey($uniqueKey)
    {
        $this->uniqueKey = $uniqueKey;

        return $this;
    }

    /**
     * Get uniqueKey
     *
     * @return string
     */
    public function getUniqueKey()
    {
        return $this->uniqueKey;
    }
}
