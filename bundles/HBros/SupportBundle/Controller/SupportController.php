<?php

namespace HBros\SupportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class SupportController extends Controller
{
    /**
     * @Route("/support/assistant")
     */
    public function indexAction()
    {
        return $this->render('HBrosSupportBundle:Assistant:assistant.html.twig');
    }
}
