<?php

namespace HBros\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MenuController extends Controller
{

    private static $adminMenu = array(
        array('text'=>'add new robot', 'uri'=>'create/robot'),
        array('text'=>'create simple page', 'uri'=>'create/simple-page'),
    );

    public function renderAdminMenuAction()
    {
        return $this->render(
            'SWDCoreBundle:Menus:menu.html.twig',
            array('links'=>self::$adminMenu)
        );

    }

}