<?php

namespace HBros\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use HBros\Commerce\ProductBundle\Entity\Robot;
use HBros\Commerce\ProductBundle\Form\CreateRobotForm;
use HBros\Commerce\ProductBundle\ProductService\ProductService;
use SWD\UtilityBundle\Service\UtilityService;
use SWD\ContentBundle\Entity\SimplePage;
use HBros\ContentBundle\Form\CreateSimplePageForm;

class AdminController extends Controller
{
    /**
     * @Route("/admin/content/list")
     */
    public function contentListAction()
    {
      return $this->render('HBrosContentBundle:Lists:list.html.twig');
    }

    /**
     * @Route("/create/robot")
     */
    public function createRobot(Request $request)
    {

        $page = array('title'=> 'Create Robot');
        $robot = new Robot();
        $form = $this->createForm(CreateRobotForm::class, $robot);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $robot = $form->getData();

            // CLEAN UP THE PATH
            $robot->setPathAlias(urlencode($robot->getPathAlias()));
            $robot->setUniqueKey(UtilityService::getRandomString(11));
            $em = $this->getDoctrine()->getManager();
            $em->persist($robot);
            $productService = new ProductService();
            $imageCollection = $form->get('images')->getData();
            $path = $this->getParameter('product_image_directory');
            $productService->persistImageCollection($robot->getUniqueKey(), $imageCollection, $path, $em);
//        return new Response(array('val'=>$val));
            $em->flush();
            $em->clear();
            return $this->redirect('/products/robots/' . $robot->getPathAlias(), 301);
        }

        return $this->render('HBrosCommerceProductBundle:Create:robot-create-form.html.twig',
            array(
                'form'=>$form->createView(),
                'page'=>$page,

            ));
    }

    /**
     * @Route("/create/simple-page")
     */
    public function createSimplePage(Request $request)
    {
        $page = array(
            'title' => 'Create a New Page',
        );


        $simplePage = new SimplePage();
        $form = $this->createForm(CreateSimplePageForm::class, $simplePage);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $robot = $form->getData();

            // CLEAN UP THE PATH
            $simplePage->setPathAlias(urlencode($simplePage->getPathAlias()));
            $simplePage->setUniqueKey(UtilityService::getRandomString(11));
            $em = $this->getDoctrine()->getManager();
            $em->persist($simplePage);
            $em->flush();
            $em->clear();
            return $this->redirect( '/' . $simplePage->getPathAlias(), 301);
        }

        return $this->render('HBrosAdminBundle:Create:simple-page-create-form.html.twig', array('page'=>$page , 'form'=>$form->createView()));
    }

}
