<?php

namespace HBros\AdminBundle\EventListener;

use SWD\AdminBundle\Event\ConfigureAdminMenuEvent;

class AdminMenuListener
{
  /**
   * @param \SWD\AdminBundle\Event\ConfigureAdminMenuEvent $event
   */

  public function onAdminMenuConfigure(ConfigureAdminMenuEvent $event)
  {
    $menu = $event->getMenu();
    $menu->addChild('Content', array('uri' => '/admin/content/list'));

  }
}